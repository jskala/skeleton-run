package sample;


import java.io.*;
import java.nio.file.*;
import java.util.List;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.*;
import javafx.stage.*;

public class Main extends Application {

    private Stage stage;
    private static Controller controller;

    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("/sample.fxml"));


        primaryStage.setTitle("Skeleton run");
        primaryStage.setScene(new Scene(root, 750, 450));
        primaryStage.setResizable(false);
        primaryStage.setOnCloseRequest((WindowEvent event) -> {
            try {


                if (controller.getCheckBoxList() == null) {
                    controller.initOfCheckboxes();
                }

                for(Thread t :controller.getThreadList()){
                    if(t.isAlive()){
                        t.stop();
                    }
                }



                saveFile();
            } catch (IOException e) {
                //Could not wirte to save file.
            }

        });
        stage = primaryStage;
        loadFile();

        primaryStage.show();
    }

    private void loadFile() throws IOException {
        File loadFile = new File("saveFile.txt");
        if(loadFile.exists()){
            List<String> linesOfFile =Files.readAllLines(loadFile.toPath());
            if(linesOfFile.size()<1){
                return;
            }
            String line = linesOfFile.get(0);
            Controller.batFile = Paths.get(line).toFile();
            controller.setBatText(line);
            if(linesOfFile.size()<2){
                return;
            }

            line = linesOfFile.get(1);
            Controller.dirFile = Paths.get(line).toFile();
            controller.setDirText(line);

            if(linesOfFile.size()<3){
                return;
            }
            line = linesOfFile.get(2);
            controller.setArgumentsTextArea(line);

            if(linesOfFile.size()<4){
                return;
            }
            line = linesOfFile.get(3);

            String[] split = line.split(" ");

            for(CheckBox box:controller.getCheckboxes(stage.getScene())){
                selectCheckBox(box,split);
            }
        }
    }

    public void selectCheckBox(CheckBox checkBox, String[] split){
        for(String module:split){
            if(module.equals((checkBox).getText())){
                checkBox.setSelected(true);
            }
        }

    }
    private void saveFile() throws IOException {

        StringBuilder sb = new StringBuilder();
        sb.append(Controller.batFile);
        sb.append("\n");
        sb.append(Controller.dirFile);
        sb.append("\n");
        sb.append(controller.getArgumentsTextArea().getText());
        sb.append("\n");
        for (CheckBox box :Controller.checkBoxList){
            if(box.isSelected()) {
                sb.append(box.getText());
                sb.append(" ");
            }
        }

        File saveFile = new File("saveFile.txt");
        Writer writer = new FileWriter(saveFile);
        writer.write(sb.toString());
        writer.close();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static void setController(Controller con){
        controller = con;
    }
}
