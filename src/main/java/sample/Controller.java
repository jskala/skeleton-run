package sample;

import java.io.*;
import java.net.URL;
import java.util.*;

import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.stage.*;

public class Controller implements Initializable {

    private final FileChooser fileChooser = new FileChooser();
    private final DirectoryChooser dirChooser = new DirectoryChooser();

    public static File batFile;
    public static File dirFile;

    public static List<CheckBox> checkBoxList;

    @FXML
    private Button fileButton;

    @FXML
    private Button stopButton;

    @FXML
    private TextField fileTextField;

    @FXML
    private TextField dirTextField;

    @FXML
    public TextArea argumentsTextArea;

    @FXML
    private Label alertLabel;



    private List<Thread> threadList = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Main.setController(this);
        stopButton.setStyle("-fx-background-color: \n"
                + "        #FFFFFF,\n"
                + "        linear-gradient(#FC5C5C, #E20909),\n"
                + "        linear-gradient(#426ab7, #263e75),\n"
                + "        linear-gradient(#eb7367, #a62b12);\n"
                + "    -fx-background-insets: 0,1,2,3;\n"
                + "    -fx-background-radius: 3,2,2,2;\n");
    }

    @FXML
    public void fileButtonPressed(MouseEvent mouseEvent) {

        deleteAlet();
        if (mouseEvent.getButton() != MouseButton.MIDDLE) {
            batFile = fileChooser.showOpenDialog(fileButton.getScene().getWindow());
            if (batFile.canWrite() && batFile.getAbsolutePath().contains(".cmd")) {
                fileTextField.setText(batFile.getAbsolutePath());
                fileTextField.positionCaret(fileTextField.getText().length());
            } else {
                alert("You must choose .cmd file.");
            }

        }
    }

    @FXML
    public void dirButtonPressed(MouseEvent mouseEvent) {
        deleteAlet();
        if (mouseEvent.getButton() != MouseButton.MIDDLE) {
            dirFile = dirChooser.showDialog(fileButton.getScene().getWindow());
            dirTextField.setText(dirFile.getAbsolutePath());
            dirTextField.positionCaret(dirTextField.getText().length());

        }
    }

    @FXML
    public void saveButtonPressed(MouseEvent mouseEvent) throws IOException {
        deleteAlet();

        if (mouseEvent.getButton() != MouseButton.MIDDLE) {
            if (dirFile == null) {
                alert("You must specify directory");
                return;
            }
            if (batFile == null) {
                alert("You must specify .cmd file");
                return;
            }
            if (checkBoxList == null) {
                initOfCheckboxes();
            }

            if (!isDirCorrect()) {
                alert("Selected directory does not contains selected modul");
                return;
            }

            ProcessBuilder pb = new ProcessBuilder("cmd", "/c", batFile.getName());
            File dir = new File(batFile.getParent());
            pb.directory(dir);
            Process p = pb.start();

        }
    }

    @FXML
    public synchronized void  startButtonPressed(MouseEvent mouseEvent) throws IOException, InterruptedException {
        deleteAlet();
        stopButton.setVisible(true);

        if (dirFile == null) {
            alert("You must specify directory");
            return;
        }

        if (checkBoxList == null) {
            initOfCheckboxes();
        }

        if (!isDirCorrect()) {
            alert("Selected directory does not contains selected modul");
            return;
        }


        Thread thread = new Thread() {

            public synchronized void run() {
                try {

                    int port = 8044;
                    for (CheckBox box : checkBoxList) {
                        if (!box.isSelected()) {
                            port++;
                            continue;
                        }

//                        Circle circle = (Circle)  box.getScene().lookup("#"+box.getId()+"Circle");

                        StringBuilder sb = new StringBuilder();
                        sb.append("start mvn -f \"");
                        sb.append(dirFile);
                        sb.append("/");
                        sb.append(box.getText());
                        sb.append("/development/");
                        sb.append(box.getText());
                        sb.append("-impl");
                        sb.append("/pom.xml\" spring-boot:run ");
                        sb.append(argumentsTextArea.getText());
                        sb.append(" -Dserver.port="+port++);
                        ProcessBuilder pb = new ProcessBuilder("cmd", "/c", sb.toString());
                        Process process = pb.start();


//                        Process p =Runtime.getRuntime().exec(System.getenv("windir") +"\\system32\\"+"tasklist.exe");
//                        BufferedReader input =  new BufferedReader(new InputStreamReader(p.getInputStream()));
//                        String pidInfo = new String();
//                        String line = new String ();
//                        while ((line = input.readLine()) != null) {
//                            if(line.contains("java")) {
//                                pidInfo +="\n";
//                                pidInfo += line;
//                            }
//                        }
//                        System.out.println(pidInfo);
//                        input.close();

                        wait(20000);



                    }
                stopButton.setVisible(false);
                } catch (InterruptedException v) {
                    System.out.println(v);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        threadList.add(thread);
        thread.start();



    }

    @FXML
    public void checkBoxPressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() != MouseButton.MIDDLE) {
            deleteAlet();

        }

    }
    @FXML
    public void stopButtonPressed(MouseEvent m ){
        stopButton.setVisible(false);
        for(Thread t :threadList){
            if(t.isAlive()){
                t.stop();
            }
        }
    }

    public void alert(String text) {
        alertLabel.setText(text);
    }

    public void deleteAlet() {
        alertLabel.setText("");
    }

    public void initOfCheckboxes() {

        checkBoxList = new ArrayList<>();
        checkBoxList.addAll(getCheckboxes(fileButton.getScene()));

    }

    private boolean isDirCorrect() {

        String[] directories = dirFile.list(new FilenameFilter() {

            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });

        for (CheckBox box : checkBoxList) {
            if (box.isSelected()) {
                boolean exists = false;
                for (String dir : directories) {
                    if (dir.contains(box.getText())) {
                        exists = true;
                        break;
                    }
                }
                if (exists == false) {
                    return false;
                } else {
                    exists = false;
                }

            }
        }
        return true;
    }

    public File getBatFile() {
        return batFile;
    }

    public File getDirFile() {
        return dirFile;
    }

    public List<CheckBox> getCheckBoxList() {
        return checkBoxList;
    }

    public void setDirText(String text) {
        dirTextField.setText(text);
    }

    public void setBatText(String text) {
        fileTextField.setText(text);

    }

    public void setArgumentsTextArea(String text){
        argumentsTextArea.setText(text);
    }

    public TextArea getArgumentsTextArea(){
        return argumentsTextArea;
    }

    public List<Thread> getThreadList() {
        return threadList;
    }

    public List<CheckBox> getCheckboxes(Scene scene){
        List<CheckBox> list = new ArrayList<>();
        boolean run = true;
        int number = 1;
        while(run){
            Node node = scene.lookup("#checkBox"+number++);
            if(node == null){
                run = false;
            }
            if(node instanceof CheckBox){
                list.add((CheckBox) node);
            }
        }
        return list;
    }
}